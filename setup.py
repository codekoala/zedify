from setuptools import setup

from zedify import version

setup(
    name=version.__name__,
    version=version.__version__,
    description='Deliver message notifications to remote machines',
    author=version.__author__,
    author_email='codekoala@cloudlery.com',
    url='http://bitbucket.org/codekoala/zedify',
    packages=['zedify'],
    entry_points={
        'console_scripts': [
            'zedifyd = zedify.remote:main',
            'zedify-conky = zedify.conky:main',
            'zedify-install-weechat = zedify.install:install_plugin',
        ]
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Environment :: X11 Applications',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2',
        'Topic :: Communications :: Chat',
    ]
)
