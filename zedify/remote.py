from collections import defaultdict

import zmq

from zedify.logger import get_logger
from zedify.config import config


class ZedifyDaemon:

    def __init__(self):
        self.unseen = defaultdict(list)
        self.log = get_logger('zedifyd')

        ctx = zmq.Context()

        self.pub = ctx.socket(zmq.PUB)
        self.pub.bind(config.zedifyd.pub)

        self.sub = ctx.socket(zmq.PULL)
        self.sub.bind(config.zedifyd.pull)

        self.rep = ctx.socket(zmq.REP)
        self.rep.bind(config.zedifyd.rep)

    def serve(self):
        """
        Simple distribution function.

        Receives zmq messages and blasts them out to any interested parties.
        """

        p = zmq.Poller()
        p.register(self.sub, zmq.POLLIN)
        p.register(self.rep, zmq.POLLIN)

        try:
            while True:
                socks = dict(p.poll())
                for sock in socks:
                    msg = sock.recv_json()
                    output = self.pub
                    self.pub.send_json(msg)

                    if sock is self.rep:
                        output = self.rep

                    self.log.info('Received message: {}'.format(msg))
                    ev = msg.get('event', None)
                    handler = getattr(self, 'handle_{}'.format(ev), None)
                    if handler is not None:
                        handler(msg, output)
                    else:
                        self.log.error('Unexpected message: {}'.format(msg))

                    if ev != 'status':
                        self.handle_status()

        except KeyboardInterrupt:
            pass
        finally:
            self.pub.close()
            self.sub.close()
            self.rep.close()

    def handle_status(self, msg=None, out=None):
        """Publish the number of unseen messages"""

        msg = msg or {}
        out = out or self.pub

        out.send_json({
            'event': 'current_status',
            'unseen': {k: len(v) for k, v in self.unseen.items() if v}
        })

        if msg.get('full', False):
            [out.send_json(m)
                for msgs in self.unseen.values()
                for m in msgs]

    def handle_typing(self, msg, out=None):
        """Reset unseen count for the buffer we're typing into"""

        self.unseen[msg['in_buffer']] = []

    def handle_buffer_switch(self, msg, out=None):
        """Reset unseen count for the buffer we're switching into"""

        self.unseen[msg['to_buffer']] = []

    def handle_mention(self, msg, out=None):
        """Increment the unseen count for the buffer"""

        self.handle_pm(msg)

    def handle_message(self, msg, out=None):
        """Increment the unseen count for the buffer"""

        self.handle_pm(msg)

    def handle_pm(self, msg, out=None):
        """Increment the unseen count for the buffer"""

        self.unseen[msg['chat']].append(msg)


def main():
    ZedifyDaemon().serve()


if __name__ == '__main__':
    main()
