import os
import sys

from zedify.config import copy_config

DIRNAME = os.path.abspath(os.path.dirname(__file__))


def install_plugin():
    """Install the weechat plugin"""

    user = os.path.expanduser('~/.weechat/python/autoload/')
    try:
        os.makedirs(user)
    except OSError:
        pass

    os.symlink(os.path.join(DIRNAME, 'weechat.py'),
               os.path.join(user, 'zedifier.py'))
    copy_config()

    sys.stdout.write('Zedifier plugin installed. Your configuration file is '
                     'at ~/.zedifyrc. You may now start weechat-curses\n')
