import logging
import os


def get_logger(name):
    params = {
        'level': logging.DEBUG,
        'format': "%(asctime)s %(levelname)s %(process)s %(message)s"
    }

    if name == 'zedifier':
        params.update({
            'filename': os.path.expanduser('~/.i3/zedifier.log'),
            'filemode': 'w',
        })

    logging.basicConfig(**params)

    return logging.getLogger(name)
