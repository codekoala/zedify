try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import SafeConfigParser as ConfigParser

import os
import sh
import shutil

from zedify.utils import DotDict


__all__ = ('config',)

_def_cfg = '/etc/default/zedify'
_user_cfg = os.path.expanduser('~/.zedifyrc')


def copy_config():
    # give the user their own copy of the configuration
    if not os.path.exists(_user_cfg):
        shutil.copyfile(_def_cfg, _user_cfg)


# pull in new configuration values from the default
_c = ConfigParser()
_c.read([_def_cfg, _user_cfg])
config = DotDict()

for section in _c.sections():
    config[section] = DotDict(_c.items(section))


try:
# get information about dbus for the notifications
    dbus_pid = int(sh.pgrep('dbus-daemon .* --nofork',
                            u=os.getlogin(),
                            f=True,
                            a=True).split()[0])
except:
    # probably not running X for notifications
    pass
else:
    env = open('/proc/{}/environ'.format(dbus_pid)).read().split('\0')
    dbus_env = dict(l.split('=', 1) for l in env
                    if l.startswith('DBUS'))
    os.environ.update(dbus_env)
