class DotDict(dict):
    """Allow access to dictionary keys with dot notation"""

    def __getattr__(self, key):
        try:
            obj = dict.__getitem__(self, key)
        except KeyError as e:
            raise AttributeError(e)

        # handle property objects
        if isinstance(obj, property):
            obj = obj.fget()

        return obj

    def __setitem__(self, key, value):
        return dict.__setitem__(self, key, value)

    __setattr__ = __setitem__

    @classmethod
    def convert(cls, obj, convert_dict=None):
        """Recursively convert the object to an DotDict"""

        if isinstance(obj, dict):
            convert_dict = convert_dict or cls.convert_dict
            out = convert_dict(cls(), obj)
        elif isinstance(obj, (list, tuple)):
            out = [cls.convert(o, convert_dict) for o in obj]

            # turn tuples back into tuples
            if isinstance(obj, tuple):
                out = tuple(out)
        else:
            out = obj

        return out

    @staticmethod
    def convert_dict(out, obj):
        """Convert a dictionary to an DotDict"""

        for key, val in obj.items():
            out[key] = out.convert(val, convert_dict=out.convert_dict)

        return out
