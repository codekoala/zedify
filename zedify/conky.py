from collections import defaultdict
import json
import random

import zmq

from zedify.config import config


def _r():
    """Return a random number between 50 and 255"""

    return random.randint(50, 255)


def generate_hex_color():
    """Generate and return a random hex color code"""

    return "#{:02x}{:02x}{:02x}".format(_r(), _r(), _r())


def main():
    """
    Talk to a zedifyd instance to get a one-shot report of the unseen messages.
    Print the message information in JSON format, suitable for use in conky for
    i3.
    """

    ctx = zmq.Context()

    # generate a random color for each nick
    colors = defaultdict(generate_hex_color)

    sock = ctx.socket(zmq.REQ)
    sock.connect(config.zedifier.req)

    sock.send_json({'event': 'status'})
    resp = sock.recv_json()
    sock.close()

    unseen = resp.get('unseen', None) or {}

    l = [{
        'full_text': "\u2605 {}: {}".format(nick, count),
        'color': colors[nick]
    } for nick, count in unseen.items()]

    if l:
        out = ',\n'.join(json.dumps(m) for m in sorted(l, key=lambda d: d['full_text']))
        print(out, ',')


if __name__ == '__main__':
    main()
