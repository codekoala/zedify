# Maintainer: Josh VanderLinden <arch@cloudlery.com>
pkgname=zedify
pkgver=56.b8a7aa9
pkgrel=1
pkgdesc="Deliver message notifications to remote machines"
arch=('any')
url="http://bitbucket.org/codekoala/zedify"
license=('MIT')
depends=('python' 'python-pyzmq' 'libnotify' 'python-sh')
optdepends=(
  'weechat: fabulous IRC client'
  'dunst: lightweight notifications'
  'xfce4-notifyd: lightweight notifications for superior WMs'
  'pulseaudio: audible notifications'
)
options=(!emptydirs)

_gitroot=https://bitbucket.org/codekoala/zedify
_gitname=zedify
_spdir=$(python -c 'import site; print(site.getsitepackages()[0])' )
_spdir2=$(python2 -c 'import site; print(site.getsitepackages()[0])' )

pkgver() {
  cd "$srcdir/$_gitname-build"
  echo $(git rev-list --count HEAD).$(git rev-parse --short HEAD)
}

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"
}

package() {
  cd "$srcdir/$_gitname-build"

  install -d ${pkgdir}/etc/py3status/plugins/
  install -d ${pkgdir}/${_spdir2}

  python setup.py install --root="$pkgdir/" --optimize=1

  cp -a zedify/resources "${pkgdir}/${_spdir}/${pkgname}/"

  install -D zedify.conf.sample "${pkgdir}/etc/default/${pkgname}"
  install -D LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

  ln -s ${_spdir}/${pkgname}/i3.py ${pkgdir}/etc/py3status/plugins/zedifier.py
  ln -s ${_spdir}/${pkgname}/ ${pkgdir}/${_spdir2}/${pkgname}
}

# vim:set ts=2 sw=2 et:
